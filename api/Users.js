export const WaitersCollection = Meteor.users;

if(Meteor.isServer) {
  Meteor.publish('Waiters', () => {
    return WaitersCollection.find({roles: 'waiter'});
  });

  Meteor.methods({
    'user.create'(options) {
      const userId = Accounts.createUser(options);
      Roles.addUsersToRoles(userId, 'client');
    },
    'user.createWaiter'(options) {
      const userId = Accounts.createUser(options);
      Roles.addUsersToRoles(userId, 'waiter');
    },
    'user.login'(data) {
      Meteor.loginWithPassword(data.username, data.password);
    }
  });
}
