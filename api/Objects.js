import {Mongo} from 'meteor/mongo';

export const ObjectsCollection = new Mongo.Collection('objects');

if(Meteor.isServer) {
  Meteor.publish('Objects', () => {
    return ObjectsCollection.find({ createdBy: this.userId });
  })

  Meteor.methods({
    'objects.add'(data) {
      data.createdBy = Meteor.userId;
      ObjectsCollection.insert(data);
    },
    'objects.remove'(data) {
      ObjectsCollection.remove(data);
    }
  });
}
