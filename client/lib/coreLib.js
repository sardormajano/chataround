//FOR DOM OPERATIONS
export const qs = document.querySelector.bind(document);
export const qsa = document.querySelectorAll.bind(document);

function containerHeight() {
    var availableHeight = $(window).height() - $('.page-container').offset().top - $('.navbar-fixed-bottom').outerHeight();

    $('.page-container').attr('style', 'min-height:' + availableHeight + 'px');
}

//FOR ADDING SCRIPT ON THE RUN
export const addScript = (attrs) => {
  if(qs(`[src='${attrs.src}']`))
    return;

  const script = document.createElement('script');

  script.setAttribute('type', 'text/javascript');

  for(key in attrs){
    script.setAttribute(key, attrs[key]);
  };

  document.head.appendChild(script);
}

export const prepareNavigationMain = () => {
  // Main navigation
  // -------------------------

  // Add 'active' class to parent list item in all levels
  $('.navigation').find('li.active').parents('li').addClass('active');

  // Hide all nested lists
  $('.navigation').find('li').not('.active, .category-title').has('ul').children('ul').addClass('hidden-ul');

  // Highlight children links
  $('.navigation').find('li').has('ul').children('a').addClass('has-ul');

  // Add active state to all dropdown parent levels
  $('.dropdown-menu:not(.dropdown-content), .dropdown-menu:not(.dropdown-content) .dropdown-submenu').has('li.active').addClass('active').parents('.navbar-nav .dropdown:not(.language-switch), .navbar-nav .dropup:not(.language-switch)').addClass('active');

  // Main navigation tooltips positioning
  // -------------------------

  // Left sidebar
  $('.navigation-main > .navigation-header > i').tooltip({
      placement: 'right',
      container: 'body'
  });

  // Collapsible functionality
  // -------------------------

  // Main navigation
  $('.navigation-main').find('li').has('ul').children('a').on('click', function (e) {
      e.preventDefault();

      // Collapsible
      $(this).parent('li').not('.disabled').not($('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).toggleClass('active').children('ul').slideToggle(250);

      // Accordion
      if ($('.navigation-main').hasClass('navigation-accordion')) {
          $(this).parent('li').not('.disabled').not($('.sidebar-xs').not('.sidebar-xs-indicator').find('.navigation-main').children('li')).siblings(':has(.has-ul)').removeClass('active').children('ul').slideUp(250);
      }
  });
}

export const prepareNavigation = () => {
  // Toggle mini sidebar
  $('.sidebar-main-toggle').on('click', function (e) {
      e.preventDefault();

      // Toggle min sidebar class
      $('body').toggleClass('sidebar-xs');
  });
}

export const prepareCollapsibles = () => {
  $('.category-collapsed').children('.category-content').hide();


  // Rotate icon if collapsed by default
  $('.category-collapsed').find('[data-action=collapse]').addClass('rotate-180');


  // Collapse on click
  $('.category-title [data-action=collapse]').click(function (e) {
      e.preventDefault();
      var $categoryCollapse = $(this).parent().parent().parent().nextAll();
      $(this).parents('.category-title').toggleClass('category-collapsed');
      $(this).toggleClass('rotate-180');

      containerHeight(); // adjust page height

      $categoryCollapse.slideToggle(150);
  });


  //
  // Panels
  //

  // Hide if collapsed by default
  $('.panel-collapsed').children('.panel-heading').nextAll().hide();


  // Rotate icon if collapsed by default
  $('.panel-collapsed').find('[data-action=collapse]').addClass('rotate-180');


  // Collapse on click
  $('.panel [data-action=collapse]').click(function (e) {
      e.preventDefault();
      var $panelCollapse = $(this).parent().parent().parent().parent().nextAll();
      $(this).parents('.panel').toggleClass('panel-collapsed');
      $(this).toggleClass('rotate-180');

      containerHeight(); // recalculate page height

      $panelCollapse.slideToggle(150);
  });
}
