import React, {Component, PropTypes} from 'react';
import QRCode from 'qrcode.react';

import {browserHistory} from 'react-router';
import {prepareNavigationMain, qs, qsa} from '../../lib/coreLib.js';
import {createContainer} from 'meteor/react-meteor-data';

import {ObjectsCollection} from '../../../api/Objects.js';

import Navbar from '../../components/Navbar.jsx';
import AdminSidebar from '../../components/AdminSidebar.jsx';
import Footer from '../../components/Footer.jsx';

class ObjectsContainer extends Component {
  componentDidMount() {
    Meteor.subscribe('Objects');
  }

  logoutHandler(e) {
    e.preventDefault();
    Meteor.logout();
  }

  modalAddObjectClickHandler(e) {
    $('.bootstrap-select').selectpicker();
  }

  addObjectClickHandler(e) {
    e.preventDefault();

    const objectNameInput = qs('#object-name-input'),
          objectTypeInput = qs('#object-type-input'),
          name = objectNameInput.value,
          type = objectTypeInput.value;

    Meteor.call('objects.add', {name, type}, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'fixed-top', 'fa-frown-o');
      }
      else {
        Bert.alert( 'Объект успешно добавлен', 'success', 'fixed-top', 'fa-frown-o');
        objectNameInput.value = '';
        objectTypeInput.value = '';
      }
    })
  }

  getObjectRows() {
    return this.props.objects.map((object) => {
      const objectType = object.type === '1' ? 'частный' : 'общий';

      return (
        <tr key={object._id}>
          <td>
            <a href="#" data-popup="lightbox">
              <QRCode value={'chataround.kz/' + object._id} />
            </a>
          </td>
          <td><a href="#">{object.name}</a></td>
          <td><a href="#">{objectType}</a></td>
          <td className="text-center">
            <ul className="icons-list">
              <li className="dropdown">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                  <i className="icon-menu9" />
                </a>
                <ul className="dropdown-menu dropdown-menu-right">
                  <li><a href="#"><i className="icon-pencil7" /> Редактировать</a></li>
                  <li><a href="#"><i className="icon-copy" /> Удалить</a></li>
                </ul>
              </li>
            </ul>
          </td>
        </tr>
      );
    });
  }

  render() {
    if(this.props.user.notReadyYet)
      return <div>загрузка...</div>;

    const props = {
      userFullName: this.props.user.profile.companyName,
      userAvatar: 'http://demo.interface.club/limitless/layout_1/LTR/material/assets/images/demo/users/face11.jpg',
      logoutHandler: this.logoutHandler.bind(this),
      userCity: 'Астана',
      pathname: this.props.location.pathname.slice(1)
    };

    const Sidebar = AdminSidebar;

    return(
      <div>
        {/* Main navbar */}
        <Navbar {...props} />
        {/* /main navbar */}
        {/* Page container */}
        <div className="page-container">
          {/* Page content */}
          <div className="page-content">
            {/* Main sidebar */}
            <Sidebar {...props}/>
            {/* /main sidebar */}
            {/* Main content */}
            <div className="content-wrapper">
              {/* Page header */}
              <div className="page-header page-header-default">
                <div className="page-header-content">
                  <div className="page-title">
                    <h4><i className="icon-arrow-left52 position-left" /> <span className="text-semibold">Chataround</span> - Объекты</h4>
                  </div>
                </div>
                <div className="breadcrumb-line">
                  <ul className="breadcrumb">
                    <li><a href="index.html"><i className="icon-home2 position-left" /> Главная</a></li>
                    <li className="active">Объекты</li>
                  </ul>
                </div>
              </div>
              {/* /page header */}
              {/* Content area */}
              <div className="content">
                {/* Media library */}
                <div className="panel panel-white">
                  <div className="panel-heading">
                    <h6 className="panel-title text-semibold">
                      Список добавленных объектов
                      <button onClick={this.modalAddObjectClickHandler.bind(this)} type="button" className="btn btn-default position-right" data-toggle="modal" data-target="#modal-add-object">
                        <i className="icon-plus-circle2 position-left" />
                        Добавить
                      </button>
                      </h6>
                  </div>
                  <table className="table table-striped media-library table-lg">
                    <thead>
                      <tr>
                        <th>QR код</th>
                        <th>Название</th>
                        <th>Тип</th>
                        <th className="text-center">Действия</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.getObjectRows()}
                    </tbody>
                  </table>
                  <div id="modal-add-object" className="modal fade">
                    <div className="modal-dialog">
                    <div className="modal-content text-center">
                      <div className="modal-header">
                        <h5 className="modal-title">Добавить объект</h5>
                      </div>
                      <form action="#" className="form">
                        <div className="modal-body">
                          <div className="form-group has-feedback">
                            <label>Название: </label>
                            <input id="object-name-input" type="text" placeholder="Введите название объекта" className="form-control" />
                          </div>
                          <div className="form-group">
                            <label>Тип объекта:</label>
                            <select id="object-type-input" className="bootstrap-select" data-width="100%">
                              <option value={1}>Частный</option>
                              <option value={2}>Общий</option>
                            </select>
                          </div>
                        </div>
                        <div className="modal-footer text-center">
                          <button onClick={this.addObjectClickHandler.bind(this)} type="submit" className="btn btn-primary">Добавить <i className="icon-plus22" /></button>
                        </div>
                      </form>
                    </div>
                    </div>
                  </div>
                </div>
                {/* /media library */}
                {/* Footer */}
                <Footer />
                {/* /footer */}
              </div>
              {/* /content area */}
            </div>
            {/* /main content */}
          </div>
          {/* /page content */}
        </div>
        {/* /page container */}
      </div>
    );
  }
}

ObjectsContainer.PropTypes = {
  user: PropTypes.object.isRequired
};

export default createContainer(() => {
  if(Meteor.user() === null)
  {
    browserHistory.push('login');
  }

  return {
    user: Meteor.user() || { notReadyYet: true },
    objects: ObjectsCollection.find().fetch()
  };
}, ObjectsContainer);
