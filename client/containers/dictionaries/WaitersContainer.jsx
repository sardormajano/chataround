import React, {Component, PropTypes} from 'react';
import QRCode from 'qrcode.react';

import {browserHistory} from 'react-router';
import {prepareNavigationMain, qs, qsa} from '../../lib/coreLib.js';
import {createContainer} from 'meteor/react-meteor-data';

import Navbar from '../../components/Navbar.jsx';
import AdminSidebar from '../../components/AdminSidebar.jsx';
import Footer from '../../components/Footer.jsx';

import {WaitersCollection} from '../../../api/Users.js';

class WaitersContainer extends Component {
  componentDidMount() {
    Meteor.subscribe('Waiters');
  }

  logoutHandler(e) {
    e.preventDefault();
    Meteor.logout();
  }

  modalAddWaiterClickHandler(e) {
    $('.bootstrap-select').selectpicker();
  }

  addWaiterClickHandler(e) {
    e.preventDefault();

    const waiterFirstNameInput = qs('#waiter-firstname-input'),
          waiterLastNameInput = qs('#waiter-lastname-input'),
          waiterEmailInput = qs('#waiter-email-input'),
          waiterPasswordInput = qs('#waiter-password-input'),

          firstname = waiterFirstNameInput.value,
          lastname = waiterLastNameInput.value,
          email = waiterEmailInput.value,
          password = waiterPasswordInput.value,

          options = {
            username: email,
            email, password,
            profile: {
              firstname,
              lastname
            }
          };

    Meteor.call('user.createWaiter', options, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'fixed-top', 'fa-frown-o');
      }
      else {
        Bert.alert( 'Официант успешно добавлен', 'success', 'fixed-top', 'fa-frown-o');
        waiterFirstNameInput.value = '';
        waiterLastNameInput.value = '';
        waiterEmailInput.value = '';
        waiterPasswordInput.value = '';
      }
    })
  }

  getWaiterRows() {
    console.log(this.props.waiters.filter((waiter) => {
      return Roles.userIsInRole(waiter._id, 'waiter');
    }));
    return this.props.waiters.filter((waiter) => {
      return Roles.userIsInRole(waiter._id, 'waiter');
    }).map((waiter) => {
      return (
        <tr key={waiter._id}>
          <td>
            <a href="#" data-popup="lightbox">
              <QRCode value={'chataround.kz/' + waiter._id} />
            </a>
          </td>
          <td><a href="#">{waiter.username}</a></td>
          <td><a href="#">{waiter.profile.firstname} {waiter.profile.lastname}</a></td>
          <td className="text-center">
            <ul className="icons-list">
              <li className="dropdown">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                  <i className="icon-menu9" />
                </a>
                <ul className="dropdown-menu dropdown-menu-right">
                  <li><a href="#"><i className="icon-pencil7" /> Редактировать</a></li>
                  <li><a href="#"><i className="icon-copy" /> Удалить</a></li>
                </ul>
              </li>
            </ul>
          </td>
        </tr>
      );
    });
  }

  render() {
    if(this.props.user.notReadyYet)
      return <div>загрузка...</div>;

    const props = {
      userFullName: this.props.user.profile.companyName,
      userAvatar: 'http://demo.interface.club/limitless/layout_1/LTR/material/assets/images/demo/users/face11.jpg',
      logoutHandler: this.logoutHandler.bind(this),
      userCity: 'Астана',
      pathname: this.props.location.pathname.slice(1)
    };

    const Sidebar = AdminSidebar;

    return(
      <div>
        {/* Main navbar */}
        <Navbar {...props} />
        {/* /main navbar */}
        {/* Page container */}
        <div className="page-container">
          {/* Page content */}
          <div className="page-content">
            {/* Main sidebar */}
            <Sidebar {...props}/>
            {/* /main sidebar */}
            {/* Main content */}
            <div className="content-wrapper">
              {/* Page header */}
              <div className="page-header page-header-default">
                <div className="page-header-content">
                  <div className="page-title">
                    <h4><i className="icon-arrow-left52 position-left" /> <span className="text-semibold">Chataround</span> - Официанты</h4>
                  </div>
                </div>
                <div className="breadcrumb-line">
                  <ul className="breadcrumb">
                    <li><a href="index.html"><i className="icon-home2 position-left" /> Главная</a></li>
                    <li className="active">Официанты</li>
                  </ul>
                </div>
              </div>
              {/* /page header */}
              {/* Content area */}
              <div className="content">
                {/* Media library */}
                <div className="panel panel-white">
                  <div className="panel-heading">
                    <h6 className="panel-title text-semibold">
                      Список официантов
                      <button onClick={this.modalAddWaiterClickHandler.bind(this)} type="button" className="btn btn-default position-right" data-toggle="modal" data-target="#modal-add-waiter">
                        <i className="icon-plus-circle2 position-left" />
                        Добавить
                      </button>
                      </h6>
                  </div>
                  <table className="table table-striped media-library table-lg">
                    <thead>
                      <tr>
                        <th>QR код</th>
                        <th>Email</th>
                        <th>Полное имя</th>
                        <th className="text-center">Действия</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.getWaiterRows()}
                    </tbody>
                  </table>
                  <div id="modal-add-waiter" className="modal fade">
                    <div className="modal-dialog">
                    <div className="modal-content text-center">
                      <div className="modal-header">
                        <h5 className="modal-title">Добавить официанта</h5>
                      </div>
                      <form action="#" className="form">
                        <div className="modal-body">
                          <div className="form-group has-feedback">
                            <label>Email: </label>
                            <input id="waiter-email-input" type="text" placeholder="Введите email официанта" className="form-control" />
                          </div>
                          <div className="form-group has-feedback">
                            <label>Имя: </label>
                            <input id="waiter-firstname-input" type="text" placeholder="Введите имя официанта" className="form-control" />
                          </div>
                          <div className="form-group has-feedback">
                            <label>Фамилия: </label>
                            <input id="waiter-lastname-input" type="text" placeholder="Введите фамилию официанта" className="form-control" />
                          </div>
                          <div className="form-group has-feedback">
                            <label>Пароль: </label>
                            <input id="waiter-password-input" type="password" placeholder="Введите пароль официанта объекта" className="form-control" />
                          </div>
                        </div>
                        <div className="modal-footer text-center">
                          <button onClick={this.addWaiterClickHandler.bind(this)} type="submit" className="btn btn-primary">Добавить <i className="icon-plus22" /></button>
                        </div>
                      </form>
                    </div>
                    </div>
                  </div>
                </div>
                {/* /media library */}
                {/* Footer */}
                <Footer />
                {/* /footer */}
              </div>
              {/* /content area */}
            </div>
            {/* /main content */}
          </div>
          {/* /page content */}
        </div>
        {/* /page container */}
      </div>
    );
  }
}

WaitersContainer.PropTypes = {
  user: PropTypes.object.isRequired
};

export default createContainer(() => {
  if(Meteor.user() === null)
  {
    browserHistory.push('login');
  }

  return {
    user: Meteor.user() || { notReadyYet: true },
    waiters: WaitersCollection.find().fetch()
  };
}, WaitersContainer);
