import React, {Component, PropTypes} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import {browserHistory} from 'react-router';
import {prepareNavigationMain, qs, qsa} from '../lib/coreLib.js';

import Home from '../components/Home.jsx';
import Navbar from '../components/Navbar.jsx';
import AdminSidebar from '../components/AdminSidebar.jsx';
import Footer from '../components/Footer.jsx';

class HomeContainer extends Component {
  logoutHandler(e) {
    e.preventDefault();
    Meteor.logout();
  }

  render() {
    if(this.props.user.notReadyYet)
      return <div>загрузка...</div>;

    Sidebar = AdminSidebar;

    const props = {
      userFullName: this.props.user.profile.companyName,
      userAvatar: 'http://demo.interface.club/limitless/layout_1/LTR/material/assets/images/demo/users/face11.jpg',
      logoutHandler: this.logoutHandler.bind(this),
      userCity: 'Астана',
      Navbar,
      Sidebar,
      Footer
    };

    return (
      <Home {...props} />
    );
  }
}

HomeContainer.propTypes = {
  user: PropTypes.object.isRequired,
}

export default createContainer(() => {
  if(Meteor.user() === null)
  {
    browserHistory.push('login');
  }

  return {
    user: Meteor.user() || { notReadyYet: true },
  };
}, HomeContainer);
