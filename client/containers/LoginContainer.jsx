import React, {Component, PropTypes} from 'react';
import {createContainer} from 'meteor/react-meteor-data';

import {browserHistory} from 'react-router';
import {addScript, qs, qsa} from '../lib/coreLib.js';

import Login from '../components/Login.jsx';
import Footer from '../components/Footer.jsx';

export default class LoginContainer extends Component {
  componentDidMount() {
    document.body.setAttribute('class', 'login-container')
  }

  loginButtonHandler(e) {
    e.preventDefault();

    const username = qs('#username-input').value,
          password = qs('#password-input').value;

    Meteor.loginWithPassword(username, password, (err) => {
      if(err) {
        Bert.alert( 'Неправильный логин или пароль!', 'danger', 'fixed-top', 'fa-frown-o');
      }
      else {
        browserHistory.push('home');

        Bert.alert( 'Добро пожаловать в Chataround!', 'success', 'fixed-top', 'fa-smile-o');
      }
    })
  }

  render() {
    return (
      <Login
        Footer={Footer}
        loginButtonHandler={this.loginButtonHandler.bind(this)}/>
    );
  }
}

LoginContainer.PropTypes = {
  user: PropTypes.object.isRequired
}

export default createContainer(() => {
  if(Meteor.user())
  {
    browserHistory.push('home');
    return ;
  }

  return {
    user: Meteor.user() || { notReadyYet: true }
  }
}, LoginContainer);
