import React, {Component, PropTypes} from 'react';
import {createContainer} from 'meteor/react-meteor-data';
import {browserHistory} from 'react-router';
import {addScript, qs, qsa} from '../lib/coreLib.js';

import Register from '../components/Register.jsx';
import Footer from '../components/Footer.jsx';

class RegisterContainer extends Component {
  componentDidMount() {
    document.body.setAttribute('class', 'login-container')
  }

  registerButtonHandler(e) {
    e.preventDefault();

    const name = qs('#name-input').value,
          email = qs('#email-input').value,
          phone = qs('#phone-input').value,
          password = qs('#password-input').value,
          options = {
            username: email,
            email, phone, password,
            profile: {
              companyName: name
            }
          };

    Meteor.call('user.create', options, (err) => {
      if(err) {
        Bert.alert( err.reason, 'danger', 'fixed-top', 'fa-frown-o');
      }
      else {
        Bert.alert( 'Акаунт успешно создан!', 'success', 'fixed-top', 'fa-smile-o');
        browserHistory.push('login');
      }
    })
  }

  render() {
    return (
      <Register
          Footer={Footer}
          registerButtonHandler={this.registerButtonHandler.bind(this)}/>
    );
  }
}

RegisterContainer.PropTypes = {
  user: PropTypes.object.isRequired
}

export default createContainer(() => {
  if(Meteor.user())
  {
    browserHistory.push('home');
    return ;
  }

  return {
    user: Meteor.user() || { notReadyYet: true }
  }
}, RegisterContainer);
