import React from 'react';

import App from './components/App.jsx';
import RegisterContainer from './containers/RegisterContainer.jsx';
import LoginContainer from './containers/LoginContainer.jsx';
import HomeContainer from './containers/HomeContainer.jsx';
import ObjectsContainer from './containers/dictionaries/ObjectsContainer.jsx';
import WaitersContainer from './containers/dictionaries/WaitersContainer.jsx';

import {Router, Route, IndexRoute, browserHistory} from 'react-router';

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={LoginContainer} />
      <Route path="login" component={LoginContainer} />
      <Route path="register" component={RegisterContainer} />
      <Route path="home" component={HomeContainer} />
      <Route path="objects" component={ObjectsContainer} />
      <Route path="waiters" component={WaitersContainer} />
    </Route>
  </Router>
);
