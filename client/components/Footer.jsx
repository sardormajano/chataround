import React, {Component} from 'react';

export default class Footer extends Component {
  render() {
    return (
      <div className="footer text-muted text-center">
        © 2015. <a href="#">Chataround</a> by <a href="#" target="_blank">Sardorbek Mamazhanov</a>
      </div>
    );
  }
}
