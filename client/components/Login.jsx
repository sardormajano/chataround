import React, {Component} from 'react';

export default class Login extends Component {
  render() {
    const {Footer, loginButtonHandler} = this.props;

    return (
      <div>
        {/* Main navbar */}
        <div className="navbar navbar-inverse bg-indigo">
          <div className="navbar-header">
            <a className="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt /></a>
            <ul className="nav navbar-nav pull-right visible-xs-block">
              <li><a data-toggle="collapse" data-target="#navbar-mobile"><i className="icon-tree5" /></a></li>
            </ul>
          </div>
          <div className="navbar-collapse collapse" id="navbar-mobile">
            <ul className="nav navbar-nav navbar-right">
              <li>
                <a href="#">
                  <i className="icon-display4" /> <span className="visible-xs-inline-block position-right"> Go to website</span>
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="icon-user-tie" /> <span className="visible-xs-inline-block position-right"> Contact admin</span>
                </a>
              </li>
              <li className="dropdown">
                <a className="dropdown-toggle" data-toggle="dropdown">
                  <i className="icon-cog3" />
                  <span className="visible-xs-inline-block position-right"> Options</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        {/* /main navbar */}
        {/* Page container */}
        <div className="page-container">
          {/* Page content */}
          <div className="page-content">
            {/* Main content */}
            <div className="content-wrapper">
              {/* Content area */}
              <div className="content">
                {/* Simple login form */}
                <form action="index.html">
                  <div className="panel panel-body login-form">
                    <div className="text-center">
                      <div className="icon-object border-slate-300 text-slate-300"><i className="icon-reading" /></div>
                      <h5 className="content-group">Авторизация<small className="display-block">Введите свои данные ниже</small></h5>
                    </div>
                    <div className="form-group has-feedback has-feedback-left">
                      <input id="username-input" type="text" className="form-control" placeholder="Username" />
                      <div className="form-control-feedback">
                        <i className="icon-user text-muted" />
                      </div>
                    </div>
                    <div className="form-group has-feedback has-feedback-left">
                      <input id="password-input" type="password" className="form-control" placeholder="Password" />
                      <div className="form-control-feedback">
                        <i className="icon-lock2 text-muted" />
                      </div>
                    </div>
                    <div className="form-group">
                      <button onClick={loginButtonHandler} type="submit" className="btn bg-pink-400 btn-block">Войти<i className="icon-circle-right2 position-right" /></button>
                    </div>
                    <div className="text-center">
                      <a href="login_password_recover.html">Забыли пароль?</a>
                    </div>
                    <div className="text-center">
                      <a href="register">Регистрация</a>
                    </div>
                  </div>
                </form>
                {/* /simple login form */}
                <Footer />
              </div>
              {/* /content area */}
            </div>
            {/* /main content */}
          </div>
          {/* /page content */}
        </div>
        {/* /page container */}
      </div>
    );
  }
}
