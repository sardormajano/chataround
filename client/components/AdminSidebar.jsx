import React, {Component} from 'react';

import {prepareNavigationMain, qs, qsa} from '../lib/coreLib.js';

import SidebarChildedLink from './SidebarChildedLink.jsx';
import SidebarLink from './SidebarLink.jsx';

export default class Sidebar extends Component {
  componentDidMount() {
    prepareNavigationMain();
  }

  render() {
    const {userAvatar, userFullName, userCity, pathname} = this.props;

    const dictionaries = {
      parentText: 'Словари',
      parentIcon: 'icon-book',
      children: [
        { href: 'objects', text: 'Объекты', className: pathname === 'objects' ? 'active' : '' },
        { href: 'waiters', text: 'Официанты', className: pathname === 'waiters' ? 'active' : ''  }
      ]
    };

    return (
      <div className="sidebar sidebar-main sidebar-default">
        <div className="sidebar-content">
          {/* User menu */}
          <div className="sidebar-user-material">
            <div className="category-content">
              <div className="sidebar-user-material-content">
                <a href="#"><img src={userAvatar} className="img-circle img-responsive" alt /></a>
                <h6>{userFullName}</h6>
                <span className="text-size-small">{userCity}</span>
              </div>
              <div className="sidebar-user-material-menu">
                <a href="#user-nav" data-toggle="collapse"><span>Мой акаунт</span> <i className="caret" /></a>
              </div>
            </div>
            <div className="navigation-wrapper collapse" id="user-nav">
              <ul className="navigation">
                <li><a href="#"><i className="icon-user" /> <span>Мой профиль</span></a></li>
              </ul>
            </div>
          </div>
          {/* /user menu */}
          {/* Main navigation */}
          <div className="sidebar-category sidebar-category-visible">
            <div className="category-content no-padding">
              <ul className="navigation navigation-main navigation-accordion">
                <SidebarChildedLink {...dictionaries} />
              </ul>
            </div>
          </div>
          {/* /main navigation */}
        </div>
      </div>
    );
  }
}
