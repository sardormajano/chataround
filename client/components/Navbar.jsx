import React, {Component} from 'react';
import {prepareNavigation, qs, qsa} from '../lib/coreLib.js';

export default class Navbar extends Component {
  componentDidMount() {
    prepareNavigation();
  }
  render() {
    const {userFullName, userAvatar, logoutHandler} = this.props;

    return (
      <div className="navbar navbar-inverse bg-indigo">
        <div className="navbar-header">
          <a className="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt /></a>
          <ul className="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i className="icon-tree5" /></a></li>
            <li><a className="sidebar-mobile-main-toggle"><i className="icon-paragraph-justify3" /></a></li>
          </ul>
        </div>
        <div className="navbar-collapse collapse" id="navbar-mobile">
          <ul className="nav navbar-nav">
            <li><a className="sidebar-control sidebar-main-toggle hidden-xs"><i className="icon-paragraph-justify3" /></a></li>
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li>
              <a href="#">
                <i className="icon-cog3" />
                <span className="visible-xs-inline-block position-right">Icon link</span>
              </a>
            </li>
            <li className="dropdown dropdown-user">
              <a className="dropdown-toggle" data-toggle="dropdown">
                <img src={userAvatar} alt />
                <span>{userFullName}</span>
                <i className="caret" />
              </a>
              <ul className="dropdown-menu dropdown-menu-right">
                <li><a href="#"><i className="icon-cog5" /> Настройки</a></li>
                <li><a onClick={logoutHandler} href="#"><i className="icon-switch2" /> Выйти</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
