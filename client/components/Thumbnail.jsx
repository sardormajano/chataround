import React, {Component} from 'react';

export default class Thumbnail extends Component {
  render() {
    const {src, title, description, moreButtonHandler} = this.props;

    <div className="col-lg-3 col-sm-6">
      <div className="thumbnail">
        <div className="thumb">
          <img src={props} alt />
          <div className="caption-overflow">
            <span>
              <a href={moreLink} onClick={moreButtonHandler} data-popup="lightbox" className="btn border-white text-white btn-flat btn-icon btn-rounded"><i className="icon-more" /></a>
            </span>
          </div>
        </div>
        <div className="caption">
          <h6 className="no-margin-top text-semibold"><a href="#" className="text-default">{title}</a></h6>
          {description}
        </div>
      </div>
    </div>
  }
}
