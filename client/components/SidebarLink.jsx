import React, {Component} from 'react';

export default class SidebarLink extends Component {
  render() {
    const {href, icon, text, className} = this.props;

    return (
      <li><a href={href}><i className={icon} /> <span>{text}</span></a></li>
    );
  }
}
