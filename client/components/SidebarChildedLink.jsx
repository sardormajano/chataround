import React, {Component} from 'react';

export default class SidebarLink extends Component {

  getChildrenJSX() {
    return this.props.children.map((child, index) => {
      if(child.divider)
        return <li key={index} className="navigation-divider" />;

      return <li className={child.className} key={index}><a href={child.href}>{child.text}</a></li>
    });
  }

  render() {
    const {parentText, parentIcon} = this.props;

    return (
      <li>
        <a href="#"><i className={parentIcon} /> <span>{parentText}</span></a>
        <ul>
          {this.getChildrenJSX()}
        </ul>
      </li>
    );
  }
}
