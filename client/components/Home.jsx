import React, {Component} from 'react';

export default class Home extends Component {
  render() {
    const {
      userFullName, userAvatar, logoutHandler,
      userCity, Navbar, Sidebar, Footer
    } = this.props;

    return (
      <div>
        {/* Main navbar */}
        <Navbar userFullName={userFullName}
                userAvatar={userAvatar}
                logoutHandler={logoutHandler} />
        {/* /main navbar */}
        {/* Page container */}
        <div className="page-container">
          {/* Page content */}
          <div className="page-content">
            {/* Main sidebar */}
            <Sidebar
                userFullName={userFullName}
                userAvatar={userAvatar}
                userCity={userCity}/>
            {/* /main sidebar */}
            {/* Main content */}
            <div className="content-wrapper">
              {/* Page header */}
              <div className="page-header page-header-default">
                <div className="page-header-content">
                  <div className="page-title">
                    <h4><i className="icon-arrow-left52 position-left" /> <span className="text-semibold">Главная</span> - Главная страница</h4>
                  </div>
                </div>
                <div className="breadcrumb-line">
                  <ul className="breadcrumb">
                    <li><a href="index.html"><i className="icon-home2 position-left" /> Главная</a></li>
                  </ul>
                </div>
              </div>
              {/* /page header */}
              {/* Content area */}
              <div className="content">
                <div className="row">
                  <div className="col-md-12">
                    {/* Basic legend */}
                    <form action="#">
                      <div className="panel panel-flat">
                        <div className="panel-heading">
                          <h5 className="panel-title">Ваши данные</h5>
                        </div>
                        <div className="panel-body">
                          <fieldset>
                            <legend className="text-semibold">Основная информация</legend>
                            <div className="form-group">
                              <label>Название:</label>
                              <input defaultValue="Fusion Night Club" type="text" className="form-control" placeholder="Наименование заведения" />
                            </div>
                            <div className="form-group">
                              <label>Адрес:</label>
                              <input defaultValue="Майлина 33" type="text" className="form-control" placeholder="Адрес заведения" />
                            </div>
                            <div className="form-group">
                              <label>Email:</label>
                              <input defaultValue="fashion@fusion.kz" type="text" className="form-control" placeholder="Email заведения" />
                            </div>
                            <div className="form-group">
                              <label>Телефон:</label>
                              <input defaultValue="+7 777 333 43 43" type="text" className="form-control" placeholder="Номер телефона заведения" />
                            </div>
                          </fieldset>
                          <fieldset>
                            <legend className="text-semibold">Дополнительная информация</legend>
                            <div className="form-group">
                              <label>Тип:</label>
                              <input defaultValue="ночной клуб" type="text" className="form-control" placeholder="тип заведения" />
                            </div>
                            <div className="form-group">
                              <label>Время работы:</label>
                              <textarea defaultValue="10:00 – 04:00 без выходных" rows={5} cols={5} placeholder="Время работы заведения" className="form-control" defaultValue={""} />
                            </div>
                          </fieldset>
                          <div className="text-right">
                            <button type="submit" className="btn btn-primary">Сохранить <i className="icon-arrow-right14 position-right" /></button>
                          </div>
                        </div>
                      </div>
                    </form>
                    {/* /basic legend */}
                  </div>
                </div>
                <Footer />
              </div>
              {/* /content area */}
            </div>
            {/* /main content */}
          </div>
          {/* /page content */}
        </div>
        {/* /page container */}
      </div>
    );
  }
}
