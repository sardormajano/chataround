import React, {Component} from 'react';

export default class Register extends Component {
  render() {
    const {Footer, registerButtonHandler} = this.props;

    return (
      <div>
        {/* Main navbar */}
        <div className="navbar navbar-inverse bg-indigo">
          <div className="navbar-header">
            <a className="navbar-brand" href="index.html"><img src="assets/images/logo_light.png" alt /></a>
            <ul className="nav navbar-nav pull-right visible-xs-block">
              <li><a data-toggle="collapse" data-target="#navbar-mobile"><i className="icon-tree5" /></a></li>
            </ul>
          </div>
          <div className="navbar-collapse collapse" id="navbar-mobile">
            <ul className="nav navbar-nav navbar-right">
              <li>
                <a href="#">
                  <i className="icon-display4" /> <span className="visible-xs-inline-block position-right"> Go to website</span>
                </a>
              </li>
              <li>
                <a href="#">
                  <i className="icon-user-tie" /> <span className="visible-xs-inline-block position-right"> Contact admin</span>
                </a>
              </li>
              <li className="dropdown">
                <a className="dropdown-toggle" data-toggle="dropdown">
                  <i className="icon-cog3" />
                  <span className="visible-xs-inline-block position-right"> Options</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        {/* /main navbar */}
        {/* Page container */}
        <div className="page-container">
          {/* Page content */}
          <div className="page-content">
            {/* Main content */}
            <div className="content-wrapper">
              {/* Content area */}
              <div className="content">
                {/* Advanced login */}
                <form action="#">
                  <div className="panel panel-body login-form">
                    <div className="text-center">
                      <div className="icon-object border-success text-success"><i className="icon-plus3" /></div>
                      <h5 className="content-group">Создать акаунт <small className="display-block">Все поля обязательны</small></h5>
                    </div>
                    <div className="content-divider text-muted form-group"><span>Данные о заведении</span></div>
                    <div className="form-group has-feedback has-feedback-left">
                      <input id="name-input" type="text" className="form-control" placeholder="Название вашего заведения" />
                      <div className="form-control-feedback">
                        <i className="icon-user-check text-muted" />
                      </div>
                    </div>
                    <div className="form-group has-feedback has-feedback-left">
                      <input id="email-input" type="text" className="form-control" placeholder="Email вашего заведения" />
                      <div className="form-control-feedback">
                        <i className="icon-mention text-muted" />
                      </div>
                    </div>
                    <div className="form-group has-feedback has-feedback-left">
                      <input id="phone-input" type="text" className="form-control" placeholder="Номер вашего мобильного телефона" />
                      <div className="form-control-feedback">
                        <i className="icon-phone text-muted" />
                      </div>
                    </div>
                    <div className="form-group has-feedback has-feedback-left">
      								<input id="password-input" type="password" className="form-control" placeholder="Выберите пароль" />
      								<div className="form-control-feedback">
      									<i className="icon-user-lock text-muted"></i>
      								</div>
      							</div>
                    <button onClick={registerButtonHandler} type="submit" className="btn bg-pink-400 btn-block btn-lg">Зарегистрироваться <i className="icon-circle-right2 position-right" /></button>
                  </div>
                </form>
                {/* /advanced login */}
                {/* Footer */}
                <Footer />
                {/* /footer */}
              </div>
              {/* /content area */}
            </div>
            {/* /main content */}
          </div>
          {/* /page content */}
        </div>
        {/* /page container */}
      </div>
    );
  }
}
